class Nucleo {
    constructor() {
        this.cuponera = "osorno";
        this.jornada = ""
        this.cuponesCliente = [];
        this.estadoCuponera = ""
        this.idSorteoActual = ""
    }


    prueba = () => {
        let timerInterval
        Swal.fire({
            title: 'Ya estas participando en los sorteos!',
            timer: 5000,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                        .textContent = Swal.getTimerLeft()
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.timer
            ) {
                console.log('I was closed by the timer')
            }
        })
    }

    validandoSorteo = (rut) => {
        rut = eliminarPuntos(rut);

        console.log("Modificando este sorteo" + this.idSorteoActual)
        if (this.idRutGanador == rut) {
            this.db.collection("sorteos").doc(this.idSorteoActual).update({
                cobrado: true,
                estado: false,
                lanzado: true
            }).then((dat3) => {
                console.log("datos")
                console.log(dat3);
                $('#divGanadorSorteo').html()
                $('#horaGanador').html(obtenerHora())
                $('#fechaGanador').html(obtenerFecha())
                $('#nombreGanador').html(this.nombreGanador)
                $('#montoGanador').html(this.premioGanador)
                $('#nombreGanador').html(this.nombreGanador)
                $('#rutGanador').html(this.idRutGanador)
                $('#divGanadorSorteo').removeAttr('hidden');
                $('#divGanadorSorteo').printThis();

                $('#botonCobrar').attr("disabled", true)
                Swal.fire({
                    title: 'Felicitaciones !',
                    type: 'success',
                    timer: 1500
                })
                this.db.collection("cuponeras").doc(this.cuponera).update({
                    pantalla: "listado",
                    cuponera: "activa",

                })

            })

        } else {
            Swal.fire({
                title: 'El rut no es el correcto',
                type: 'warning',
                timer: 3500
            })
        }
    }
    sincronizarCuponera = () => {
        this.db.collection("cuponeras")
            .doc(this.cuponera)
            .onSnapshot((querySnapshot) => {
                this.jornada = "";
                this.premioActual = "";
                this.jornada = querySnapshot.data().jornada
                this.estadoCuponera = querySnapshot.data().cuponera
                this.estado = querySnapshot.data().estado
                this.idSorteoActual = querySnapshot.data().idSorteoActual
                this.idRutGanador = querySnapshot.data().rut
                this.nombreGanador = querySnapshot.data().nombre
                this.premioGanador = querySnapshot.data().premio
                this.valorMinuto = querySnapshot.data().valorMinuto
                this.teclado = querySnapshot.data().teclado
                console.log(this.estadoCuponera + "estado")
                if (this.estado == true) {

                    switch (this.estadoCuponera) {
                        case "inactiva":
                            cargar("iniciar.html")
                            break;
                        case "activa":
                            if (this.teclado == true) {

                                cargar("botonera.html")
                            } else {
                                cargar("preparando.html")
                            }

                            break;
                        case "validando":
                            cargar("validando.html")
                            break;
                        case "sorteando":
                            cargar("sorteando.html")
                            break;
                        case "minutoFeliz":
                            cargar("minutoFeliz.html")
                            break;
                        case "iniciando":

                            console.log("iniciando")
                            cargar("iniciando.html")
                            break;

                        default:
                            break;

                    }
                }
                else {

                    cargar("iniciar.html")

                }



            })


    }
    sincronizarJornada = () => {
        this.db.collection("jornadasCuponeras")
            .get()
            .doc(this.cuponera)
            .then((querySnapshot) => {

            })


    }
    iniciar = () => {
        this.develop = {
            apiKey: "AIzaSyBEf9-h8CuW9cNjiJo8uLnMfbcBjgPxG9Y",
            authDomain: "developchamaclub.firebaseapp.com",
            databaseURL: "https://developchamaclub.firebaseio.com",
            projectId: "developchamaclub",
            storageBucket: "developchamaclub.appspot.com",
            messagingSenderId: "326557585019",
            appId: "1:326557585019:web:30b4aea596eb27fa74dcde"
        };

        this.produccion = {
            apiKey: "AIzaSyC2dxeazyHNSTAuimIhFaVXt8AYZV3aub4",
            authDomain: "sorteoschama.firebaseapp.com",
            databaseURL: "https://sorteoschama.firebaseio.com",
            projectId: "sorteoschama",
            storageBucket: "sorteoschama.appspot.com",
            messagingSenderId: "214342016835",
            appId: "1:214342016835:web:68bede8357c4936f29d9f6"
        };
        this.firebase = firebase.initializeApp(this.develop);
        this.db = this.firebase.firestore();
        this.categorias = new Categorias();
        this.categorias.iniciar();
        this.sincronizarCuponera();
    }


    iniciarJornada = () => {
        this.password = prompt("Para iniciar ingrese la pass");
        if (this.password == "8521") {
            this.db.collection("jornadaCuponeras").add({
                monto: 0
            }).then((dato) => {
                this.jornada = dato.id;
                this.cuponeraEdit = this.db.collection("cuponeras").doc(this.cuponera);
                return this.cuponeraEdit.update({
                    cuponera: "iniciando",
                    jornada: this.jornada,
                    nombre: "",
                    fecha: obtenerFecha(),
                    rut: "",
                    id: "",
                    estado: true
                }).then(() => {
                    this.db.collection("jornadaCuponeras").add({
                        monto: 0
                    }).then((dato) => {
                        this.jornada = dato.id;
                        console.log("borrandoSorteos")
                        this.iniciarCupones();
                        this.borrarSorteos()
                    })
                })
            })
        } else {

        }

    }


    borrarSorteos = () => {
        this.db.collection("sorteos").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;
                this.db.collection("sorteos").doc(this.llave).delete()
            })
        })
    }

    iniciarCupones = () => {

        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {

                this.llave = datos.id;
                if (datos.data().sucursal == this.cuponera) {
                    this.db.collection("clientes").doc(this.llave).update({
                        estado: false,
                        cupon: false,
                        monto: 0,
                        fecha: "",
                        hora: ""
                    })
                }

            })
            console.log("iniciandoiCuponera")
            this.cuponeraEdit = this.db.collection("cuponeras").doc(this.cuponera);
            return this.cuponeraEdit.update({
                cuponera: "activa",
                jornada: this.jornada,
                nombre: "",
                rut: "",
                id: ""
            }).then(() => {

            })
        })
    }

    reiniciarGanador = (sucursal) => {
        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;

                this.db.collection("clientes").doc(this.llave).update({
                    ganador: false
                })


            })
        })
    }

    reiniciarMinuto = (sucursal) => {
        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;

                this.db.collection("clientes").doc(this.llave).update({
                    minuto: false
                })


            })
        })
    }
    setearDeshabilitados = () => {
        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;
                this.db.collection("clientes").doc(this.llave).update({
                    deshabilitado: false,
                    cupon: false
                })
            })
        })
    }
    reiniciarTodos = () => {
        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;
                if (datos.data().sucursal == "osorno") {
                    console.log(datos.data())
                    this.db.collection("clientes").doc(this.llave).update({
                        cupon: false
                    })
                }

            })
        })
    }
    asignarMinuto = (sucursal) => {
        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;
                if (datos.data().sucursal == sucursal) {

                    this.db.collection("clientes").doc(this.llave).update({
                        minuto: false
                    })
                }

            })
        })
    }
    asignarGanador = (sucursal) => {
        this.db.collection("clientes").get().then((data) => {
            data.forEach((datos) => {
                this.llave = datos.id;
                if (datos.data().sucursal == sucursal) {

                    this.db.collection("clientes").doc(this.llave).update({
                        ganador: false
                    })
                }

            })
        })
    }
    iniciarCoyhaique = () => {

    }
    cargarDatosMinuto = () => {

        this.cupRef = this.db.collection("cuponeras").doc(this.cuponera)
        this.cupRef.get().then((datos) => {
            this.valorMinuto = datos.data().valorMinuto;
            return this.cupRef.update({
                jornada: '',
                cuponera: "minutoFeliz"
            }).then(function () {

            })
        })

    }
    iniciarMinuto = () => {
        this.password = prompt("Para Iniciar MinutoFeliz ingrese la pass");
        if (this.password == "8521") {
            $('.modal-backdrop').attr('class', '');
            $('#divResumen').attr('hidden', true);
            nucleo.cargarDatosMinuto()

        } else {
            Swal.fire({
                title: 'PassIncorrecta',
                type: 'warning',
                timer: 1500
            })
        }
    }

    cerrarMinuto = () => {
        this.password = prompt("Para Cerrar MinutoFeliz ingrese la pass");
        if (this.password == "8521") {

            $('.modal-backdrop').attr('class', '');
            $('#divResumen').attr('hidden', true);
            this.cupRef = this.db.collection("cuponeras").doc(this.cuponera)
            return this.cupRef.update({
                jornada: '',
                cuponera: "activa"
            }).then(function () {

            })
        } else {
            Swal.fire({
                title: 'PassIncorrecta',
                type: 'warning',
                timer: 1500
            })
        }
    }
    cerrarDia = () => {
        this.password = prompt("Para Cerrar e imprimir ingrese la pass");
        if (this.password == "8521") {
            $('#divResumen').attr('hidden', true);
            this.cuponeraEdit = this.db.collection("cuponeras").doc(this.cuponera);
            return this.cuponeraEdit.update({
                jornada: '',
                cuponera: "inactiva"
            }).then(function () {

            })
        } else {
            Swal.fire({
                title: 'PassIncorrecta',
                type: 'warning',
                timer: 1500
            })
        }
    }
    imprimir = () => {
        this.password = prompt("Para  imprimir ingrese la pass");
        if (this.password == "3891") {


        } else {
            Swal.fire({
                title: 'PassIncorrecta',
                type: 'warning',
                timer: 1500
            })
        }
    }
    calcularResumen = () => {

        this.db.collection("poolCupones/").where("jornada", "==", this.jornada)
            .get()
            .then((querySnapshot) => {
                this.bronce = 0;
                this.silver = 0;
                this.golden = 0;
                this.platinium = 0;
                this.contadorBronce = 0;
                this.contadorSilver = 0;
                this.contadorGolden = 0;
                this.contadorPlatinium = 0;
                this.cuponesCliente = [];
                querySnapshot.forEach((datos) => {
                    this.cuponesCliente.push(datos.data())
                    switch (datos.data().clasificacion) {
                        case "Bronce":
                            this.bronce += parseInt(datos.data().monto)
                            this.contadorBronce++;
                            break;
                        case "Silver":
                            this.silver += parseInt(datos.data().monto)
                            this.contadorSilver++;
                            break;
                        case "Golden":
                            this.golden += parseInt(datos.data().monto)
                            this.contadorGolden++;
                            break;
                        case "Platinium":
                            this.platinium += parseInt(datos.data().monto)
                            this.contadorPlatinium++;
                            break;

                        default:
                            break;
                    }


                })
                this.total = parseInt(this.bronce) + parseInt(this.silver) + parseInt(this.golden) + parseInt(this.platinium)
                $('#totalCupones').html(puntos(this.total))
                $('#cantidadBronce').html("X" + this.contadorBronce)
                $('#acumuladorBronce').html(puntos(this.bronce))
                $('#cantidadSilver').html("X" + this.contadorSilver)
                $('#acumuladorSilver').html(puntos(this.silver))
                $('#cantidadGolden').html("X" + this.contadorGolden)
                $('#acumuladorGolden').html(puntos(this.golden))
                $('#cantidadPlatinium').html("X" + this.contadorPlatinium)
                $('#acumuladorPlatinium').html(puntos(this.platinium))


                $('#horaResumen').html(obtenerHora())
                $('#fechaResumen').html(obtenerFecha())
                $('#divResumen').removeAttr('hidden');
                $('#divResumen').printThis();
                setTimeout(() => {
                    $('#divResumen').attr('hidden', true);
                    location.reload();
                }, 3000);



            })
    }

    actualizarClientes = (cliente) => {
        this.clienteEdit = this.db.collection("clientes").doc(cliente);
        return this.clienteEdit.update({
            hora: obtenerHora(),
            fecha: obtenerFecha()
        }).then(function () {
            console.log("cliente actualizado")
        })
    }
    solicitarMinuto = (rut) => {
        $("#boton").attr("disabled", true);
        this.clienteActual = "";
        rut = eliminarPuntos(rut);
        this.cuponesCliente = [];
        this.db.collection("clientes").where("rut", "==", rut)
            .get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {

                    this.clienteActual = doc.data();
                    this.clienteActual.id = doc.id;
                    if (doc.data().minuto == false) {
                        this.cuponesCliente.push(doc.data())
                    }
                });
                if (this.clienteActual == "") {
                    $("#boton").removeAttr("disabled");
                    Swal.fire({
                        title: 'Rut no encontrado',
                        type: 'warning',
                        timer: 1500
                    })
                } else {

                    if (this.cuponesCliente.length > 0) {
                        if (this.cuponesCliente.ganador == false) {
                            this.state = true
                        } else {
                            this.state = false
                        }
                        this.categorias.filtrar(this.clienteActual.clasificacion);
                        this.db.collection("poolCupones/").add({
                            nombre: this.clienteActual.nombre,
                            apellido: this.clienteActual.apellido,
                            rut: this.clienteActual.rut,
                            id: this.clienteActual.id,
                            fecha: obtenerFecha(),
                            hora: obtenerHora(),
                            monto: this.categorias.categoriaFiltrada[0].premio,
                            clasificacion: this.clienteActual.clasificacion,
                            estado: this.state,
                            local: this.cuponera,
                            jornada: this.jornada
                        })
                        this.db.collection("jornadasCuponeras/" + this.jornada + "/cupones").add({
                            nombre: this.clienteActual.nombre,
                            apellido: this.clienteActual.apellido,
                            rut: this.clienteActual.rut,
                            id: this.clienteActual.id,
                            fecha: obtenerFecha(),
                            hora: obtenerHora(),
                            monto: this.categorias.categoriaFiltrada[0].premio,
                            clasificacion: this.clienteActual.clasificacion,
                            estado: this.state,
                            local: this.cuponera
                        }).then(() => {
                            this.actualizarClientes(this.clienteActual.id);
                            $('#nombreCupon').html(this.clienteActual.nombre + " " + this.clienteActual.apellido)
                            $('#rutCupon').html(this.clienteActual.rut.toLowerCase())
                            $('#horaCupon').html(obtenerHora());
                            $('#fechaCupon').html(obtenerFecha());
                            $('#horaSorteo').html(obtenerHora());
                            $('#fechaSorteo').html(obtenerFecha());
                            $('#clasificacionCupon').html(this.clienteActual.clasificacion);
                            $('#montoCupon').html(puntos(this.valorMinuto))
                            $('#nombreSorteo').html(this.clienteActual.nombre + " " + this.clienteActual.apellido)
                            $('#apellidoSorteo').html(this.clienteActual.apellido)
                            $('#rutSorteo').html(this.clienteActual.rut.toLowerCase())
                            $('#divSorteo').removeAttr('hidden');
                            $('#rut').val("")
                            $('#divSorteo').printThis();


                            Swal.fire({
                                title: 'Ya estas participando en los sorteos!',
                                timer: 5000,

                            })
                            setTimeout(() => {
                                $("#boton").removeAttr("disabled");
                                $('#divSorteo').attr('hidden', true);
                                this.db.collection("clientes").doc(this.clienteActual.id).update({
                                    estado: true,
                                    fecha: obtenerFecha(),
                                    hora: obtenerHora(),
                                    monto: this.categorias.categoriaFiltrada[0].premio,
                                    minuto: true
                                })


                            }, 2000);



                        })

                    } else {

                        Swal.fire({
                            title: 'Ustéd ya obtuvo su cupon diario, vuelva mañana!',
                            type: 'warning',
                            timer: 3500
                        })
                        $("#boton").removeAttr("disabled");
                        $('#rut').val("")
                    }





                }
            })
            .catch((error) => {
                console.log("Error getting documents: ", error);
            });

    }
    solicitarCupon = (rut) => {
        $("#boton").attr("disabled", true);
        this.clienteActual = "";
        rut = eliminarPuntos(rut);
        this.cuponesCliente = [];
        this.db.collection("clientes").where("rut", "==", rut)
            .get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    this.clienteActual = doc.data();
                    this.clienteActual.id = doc.id;
                    if (doc.data().cupon == false) {
                        this.cuponesCliente.push(doc.data())
                    }
                });
                if (this.clienteActual == "") {
                    $("#boton").removeAttr("disabled");
                    Swal.fire({
                        title: 'Rut no encontrado',
                        type: 'warning',
                        timer: 1500
                    })
                } else {

                    if (this.cuponesCliente.length > 0) {
                        if (this.cuponesCliente.ganador == false) {
                            this.state = true
                        } else {
                            this.state = false
                        }
                        this.categorias.filtrar(this.clienteActual.clasificacion);

                        this.db.collection("poolCupones/").add({
                            nombre: this.clienteActual.nombre,
                            apellido: this.clienteActual.apellido,
                            rut: this.clienteActual.rut,
                            id: this.clienteActual.id,
                            fecha: obtenerFecha(),
                            hora: obtenerHora(),
                            monto: this.categorias.categoriaFiltrada[0].premio,
                            clasificacion: this.clienteActual.clasificacion,
                            estado: this.state,
                            local: this.cuponera,
                            jornada: this.jornada
                        })
                        this.db.collection("jornadasCuponeras/" + this.jornada + "/cupones").add({
                            nombre: this.clienteActual.nombre,
                            apellido: this.clienteActual.apellido,
                            rut: this.clienteActual.rut,
                            id: this.clienteActual.id,
                            fecha: obtenerFecha(),
                            hora: obtenerHora(),
                            monto: this.categorias.categoriaFiltrada[0].premio,
                            clasificacion: this.clienteActual.clasificacion,
                            estado: this.state,
                            local: this.cuponera
                        }).then(() => {
                            this.actualizarClientes(this.clienteActual.id);
                            $('#nombreCupon').html(this.clienteActual.nombre + " " + this.clienteActual.apellido)
                            $('#rutCupon').html(this.clienteActual.rut.toLowerCase())
                            $('#horaCupon').html(obtenerHora());
                            $('#fechaCupon').html(obtenerFecha());
                            $('#horaSorteo').html(obtenerHora());
                            $('#fechaSorteo').html(obtenerFecha());
                            $('#clasificacionCupon').html(this.clienteActual.clasificacion);
                            $('#montoCupon').html(puntos(this.categorias.categoriaFiltrada[0].premio))
                            $('#nombreSorteo').html(this.clienteActual.nombre + " " + this.clienteActual.apellido)
                            $('#apellidoSorteo').html(this.clienteActual.apellido)
                            $('#rutSorteo').html(this.clienteActual.rut.toLowerCase())
                            $('#divSorteo').removeAttr('hidden');
                            $('#rut').val("")
                            $('#divSorteo').printThis();


                            Swal.fire({
                                title: 'Ya estas participando en los sorteos!',
                                timer: 5000,

                            })
                            setTimeout(() => {
                                $("#boton").removeAttr("disabled");
                                $('#divSorteo').attr('hidden', true);
                                this.db.collection("clientes").doc(this.clienteActual.id).update({
                                    estado: true,
                                    cupon: true,
                                    fecha: obtenerFecha(),
                                    hora: obtenerHora(),
                                    monto: this.categorias.categoriaFiltrada[0].premio
                                })


                            }, 2000);



                        })

                    } else {

                        Swal.fire({
                            title: 'Ustéd ya obtuvo su cupon diario, vuelva mañana!',
                            type: 'warning',
                            timer: 3500
                        })
                        $("#boton").removeAttr("disabled");
                        $('#rut').val("")
                    }





                }
            })
            .catch((error) => {
                console.log("Error getting documents: ", error);
            });

    }
}